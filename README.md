# Parser for [Adam13531](https://www.twitch.tv/adam13531)

This parser was created for [Adam13531](https://www.twitch.tv/adam13531) a streamer on twitch, who creates the game Botland. 
This parser is for backend stuff.

## How to use

``` node parser.js <FILEPATH>```

## Demo

### Input

```
REPAIR:3;ARMOR:3;REGEN:1
ARTILLERY:1;THRUSTERS:2;REFLECT:1
LASERS:3;ARTILLERY:1;MELEE:1;THRUSTERS:1;LANDMINES:1
MELEE:3;THRUSTERS:3;CLOAKING:1
MISSILES:3;ARTILLERY:3;ARMOR:1
ARTILLERY:1;SHIELD:2
SHIELD:1;INFERNO_ZAPPER:1;EMP:3;VAMPIRIC_LASERS:1
LANDMINES:3;REFLECT:1;TELEPORT:2
...
```

### Output

```
ARTILLERY             total: 229 (14.70%)
   Level 3: 142 (62.01%)
   Level 1: 56 (24.45%)
   Level 2: 31 (13.54%)
MELEE                 total: 176 (11.30%)
   Level 3: 106 (60.23%)
   Level 1: 41 (23.30%)
   Level 2: 29 (16.48%)
...
```