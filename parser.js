var fs = require('fs');

if (process.argv.length > 2) {
    var filePath = process.argv[2];
    if (filePath.indexOf('.') == 0 && fs.existsSync(process.cwd + '/' + filePath)) {
        listFileContents(process.cwd + '/' + filePath);
    } else if (fs.existsSync(filePath)) {
        listFileContents(filePath);
    } else {
        console.log('The file you entered does not exist!');
    }
} else {
    console.log('Just enter file path after:');
}

// Do analysis
function listFileContents(filePath) {
    var readline = require('readline');

    var instream = fs.createReadStream(filePath);

    var rd = readline.createInterface({
        input: instream,
        output: process.stdout,
        terminal: false
    });

    var data = [];
    var total = 0;

    rd.on('line', function (line) {
        // Separate line
        var parts = line.split(/;/g);

        for (var i = 0; i < parts.length; i++) {
            // Separate part in component and level
            var atoms = parts[i].split(/:/);

            if (atoms.length == 2) {
                if (data.filter(function (e) {
                    if (e.name == atoms[0]) {
                        e.overall += 1;
                        total++;

                        // Find level
                        if (e.levels.filter(function (e) {
                            if (e.name == atoms[1]) {
                                e.total += 1;
                            }

                            return e.name == atoms[1];
                        }).length <= 0) {
                            e.levels.push({
                                name: atoms[1],
                                total: 1
                            });
                        }
                    }

                    return e.name == atoms[0];
                }).length <= 0) {
                    data.push({
                        name: atoms[0],
                        overall: 1,
                        levels: [
                            { name: atoms[1], total: 1 }
                        ]
                    });
                }
            }
        }
    });

    instream.on('end', () => {
        // Sort data
        data.sort(function (a, b) {
            return b.overall - a.overall;
        });

        // Print data out
        for (var i = 0; i < data.length; i++) {
            var elem = data[i];

            if (elem.overall != null) {
                console.log(elem.name + spacer(elem.name, 20) + '  total: ' + data[i].overall + ' (' + percentage(elem.overall, total) + ')');

                // Sort levels bei total desc
                elem.levels.sort(function (a, b) {
                    return b.total - a.total;
                });

                for (var j = 0; j < elem.levels.length; j++) {
                    var level = elem.levels[j];
                    console.log('   Level ' + level.name + ': ' + level.total + ' (' + percentage(level.total, elem.overall) + ')');
                }
            }
        }
    });

    function spacer(word, maxLength) {
        var retVal = '';

        for (var i = 0; i < maxLength - word.length; i++) {
            retVal += ' ';
        }

        return retVal;
    }

    function percentage(part, total) {
        return ((part / total) * 100).toFixed(2) + '%';
    }
}